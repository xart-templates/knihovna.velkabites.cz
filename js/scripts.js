$.noConflict();
jQuery(document).ready(function ($) {

    /**
     * Modular tabs
     * @author Tomas Vondracek
     * @date 13.11.2016
     */
    var tabs = (function () {
        var $tabs = $(".tabs");
        var $link = $tabs.find("nav a");
        var $activeLink = $tabs.find("nav a.active").attr("href");
        var $content = $tabs.find(".tabs-content");
        var $contentByLink = $content.find($activeLink);

        function _bindEvents() {
            $link.on("click", _renderTab);
        }

        function _renderTab(event) {
            event.preventDefault();
            _toggleClass(this);
        }

        function _toggleClass(element) {
            $link.removeClass("active");
            $clickedLink = $(element);
            $clickedLink.addClass("active");
            $content.children().removeClass("active").css({"display": "none"});
            $content.find($clickedLink.attr("href")).addClass("active").css({"display": "block"});
        }

        function _defaultStyles() {
            $content.children().removeClass("active").css({"display": "none"});
            $contentByLink.addClass("active").css({"display": "block"});
        }

        function _isReady() {
            if ($link.hasClass("active")) {
                if (!$contentByLink.length) {
                    console.log("%c" + $activeLink + "%c not found in .tabs-content", "color:#f00", "color:inherit");
                    return false;
                }
            } else {
                console.log("Define %c.active%c class on link in 'nav.tabs-navigation'", "color:#0f0", "color:inherit");
                return false;
            }
            return true;
        }

        function initTabs() {
            if (_isReady()) {
                _defaultStyles();
                _bindEvents();
            }
        }

        return {
            init: initTabs
        }

    })();
    tabs.init();
//./tabs

    /**
     * Font changer
     * @author Tomas Vondracek
     * @date 13.11.2016
     */
    var fontChanger = (function () {
        var $mod_fontChanger = $(".mod_custom-fontChanger");
        var $small = $mod_fontChanger.find(".small");
        var $medium = $mod_fontChanger.find(".medium");
        var $big = $mod_fontChanger.find(".big");
        var elements = ["b", "strong", "i", "em", "mark", "small", "del", "ins", "sub", "sup", "article", "section",
            "a", "nav", "div", "span", "input", "ul", "li", "button", "form", "label", "footer", "header", "figure", "p", "h1",
            "h2", "h3", "h4", "h5", "h6", "time", "textarea", "select", "dl", "dt", "dd"
        ];

        function _getFontSize(element) {
            return parseInt(element.css("fontSize"));
        }

        function _bindEvents() {
            $small.on("click", _smallText);
            $medium.on("click", _mediumText);
            $big.on("click", _bigText);
        }

        function _changeFontSize(percentage) {
            for (var i = 0; i < elements.length; i++) {
                $this = $(elements[i]);
                if ($this.hasClass("fontSizeFixed") || $this.parents(".fontSizeFixed").length) continue;
                if (percentage != 0) var fontSize = (_getFontSize($this) * percentage) / 100;
                $this.css("fontSize", percentage != 0 ? fontSize : "");
            }
        }

        function _smallText() {
            _changeFontSize(90)
        }

        function _mediumText() {
            _changeFontSize(0);
        }

        function _bigText() {
            _changeFontSize(120);
        }

        function initEvents() {
            _bindEvents();
        }

        return {
            init: initEvents
        }

    })();
    fontChanger.init();


    /*
     function createCSSSelector(selector, style) {
     if (!document.styleSheets) return;
     if (document.getElementsByTagName('head').length == 0) return;

     var styleSheet, mediaType;

     if (document.styleSheets.length > 0) {
     for (var i = 0, l = document.styleSheets.length; i < l; i++) {
     if (document.styleSheets[i].disabled)
     continue;
     var media = document.styleSheets[i].media;
     mediaType = typeof media;

     if (mediaType === 'string') {
     if (media === '' || (media.indexOf('screen') !== -1)) {
     styleSheet = document.styleSheets[i];
     }
     }
     else if (mediaType == 'object') {
     if (media.mediaText === '' || (media.mediaText.indexOf('screen') !== -1)) {
     styleSheet = document.styleSheets[i];
     }
     }

     if (typeof styleSheet !== 'undefined')
     break;
     }
     }

     if (typeof styleSheet === 'undefined') {
     var styleSheetElement = document.createElement('style');
     styleSheetElement.type = 'text/css';
     document.getElementsByTagName('head')[0].appendChild(styleSheetElement);

     for (i = 0; i < document.styleSheets.length; i++) {
     if (document.styleSheets[i].disabled) {
     continue;
     }
     styleSheet = document.styleSheets[i];
     }

     mediaType = typeof styleSheet.media;
     }

     if (mediaType === 'string') {
     for (var i = 0, l = styleSheet.rules.length; i < l; i++) {
     if (styleSheet.rules[i].selectorText && styleSheet.rules[i].selectorText.toLowerCase() == selector.toLowerCase()) {
     styleSheet.rules[i].style.cssText = style;
     return;
     }
     }
     styleSheet.addRule(selector, style);
     }
     else if (mediaType === 'object') {
     var styleSheetLength = (styleSheet.cssRules) ? styleSheet.cssRules.length : 0;
     for (var i = 0; i < styleSheetLength; i++) {
     if (styleSheet.cssRules[i].selectorText && styleSheet.cssRules[i].selectorText.toLowerCase() == selector.toLowerCase()) {
     styleSheet.cssRules[i].style.cssText = style;
     return;
     }
     }
     styleSheet.insertRule(selector + '{' + style + '}', styleSheetLength);
     }

     }*/


    //Datepicker
    $.datepicker.regional['cs'] = {
        closeText: 'Cerrar',
        prevText: 'Předchozí',
        nextText: 'Další',
        currentText: 'Hoy',
        monthNames: ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'],
        monthNamesShort: ['Le', 'Ún', 'Bř', 'Du', 'Kv', 'Čn', 'Čc', 'Sr', 'Zá', 'Ří', 'Li', 'Pr'],
        dayNames: ['Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota'],
        dayNamesShort: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
        dayNamesMin: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
        weekHeader: 'Sm',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };

    $.datepicker.setDefaults($.datepicker.regional['cs']);

    $(".datepicker").datepicker({
        inline: true,
        showOtherMonths: true,
        minDate: 0
    });

    //apply styles to current week
    $tr = $("[class*='current-day']").parents("tr");
    $td = $tr.find("td");
    for (i = 0; i < $td.length; i++) {
        $($td[i]).find("a").css({
            color: "#0575c7",
            fontWeight: "bold"
        });
    }

    //./datepicker

    //slick init
    $('.slider').slick({
        autoplay: true,
        autoplaySpeed: 1500,
        infinite: true,
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev"></button>',
        nextArrow: '<button type="button" class="slick-next"></button>',
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 550,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 380,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    //./slick

    /**
     * Handles non-exist hover on mobile by supplementing touchend gesture, also this can work by double click as same as on mobile touch
     */
    $mainLink = $(".mod_menu-submenu .menu>li>a");
    $getSubList = $(".menu li ul");
    var count = 0;
    $mainLink.on(
        'touchend click', function (event) {
            event.stopPropagation();
            event.preventDefault();
            ++count;
            if (count % 2 != 0) {
                $(this).parent().find("ul").removeClass("active").css("display", "none");
            } else {
                $(this).parent().find("ul").addClass("active").css("display", "block");
                $('html, body').animate({
                    scrollTop: ($('ul.active').parent().offset().top)
                }, 500);
            }
        });


    var go_top = function () {
        var offset = $(window).scrollTop();
        if (offset > 100) {
            $('#gotop').addClass("on");
        } else {
            $('#gotop').removeClass('on');
        }
    };
    go_top();
    $(window).scroll(function () {
        go_top();
    });
    $('#gotop a').click(function () {
        $('html,body').animate({scrollTop: 0}, 600);
        return false;
    });


    // checkbox, radio style
    $('[type="checkbox"],[type="radio"]').wrap('<span class="checkbox_wrap"/>').after('<span class="checkbox_btn"/>');

    // select box
    $('select:not([multiple="multiple"]), select:not([multiple]), select:not([multiple=\'multiple\'])').wrap('<span class="select_wrap"/>');


    // file input style
    $('input[type=file]').each(function () {
        var uploadText = $(this).data('value');
        if (uploadText) {
            var uploadText = uploadText;
        } else {
            var uploadText = 'Choose file';
        }
        var inputClass = $(this).attr('class');
        $(this).wrap('<span class="fileinputs"></span>');
        $(this)
            .parents('.fileinputs')
            .append($('<span class="fakefile"/>')
                .append($('<input class="input-file-text" type="text" />')
                    .attr({
                        'id': $(this)
                            .attr('id') + '__fake', 'class': $(this).attr('class') + ' input-file-text'
                    }))
                .append('<input type="button" value="' + uploadText + '">'));
        $(this)
            .addClass('type-file')
            .css('opacity', 0);
        $(this)
            .bind('change mouseout', function () {
                $('#' + $(this).attr('id') + '__fake')
                    .val($(this).val().replace(/^.+\\([^\\]*)$/, '$1'))
            })
    });

    // placeholder
    if (document.createElement('input').placeholder == undefined) {
        $('[placeholder]').focus(function () {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder')
            }
        }).blur(function () {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'))
            }
        }).blur()
    }
    $('table.normal').each(function (index, el) {
        // vetsi tabulka - povol zalamovani
        if ($(this).find('th').length > 1) {
            $(this).basictable({
                tableWrapper: true
            });
        }
        // mensi tabulka - nastav breakpoint na maly rozmer
        else {
            $(this).basictable({
                tableWrapper: true,
                breakpoint: 120 // css media query
            });
        }
    });

});